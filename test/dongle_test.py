# pylint: skip-file
# type: ignore

from sipyconfig import SiUSBStation
from sipyconfig.utils import *
from sipyconfig.enums import *

si = SiUSBStation(mode_direct=True)

print(f"mode_master: {si._direct_mode}")
print(f"mode: {si.mode_string}")
print(f"control_number: {si.control_number}")
print("STATION_MODE", si.model_id_string)

try:
    while True:
        try:
            card = si.wait_for_si_card()
            print_si_card(card)
        except Exception as e:
            print(e)
finally:
    si.flush()
