# pylint: skip-file
# type: ignore

import time
from typing import TYPE_CHECKING

from sipyconfig import SiUSBStation
from sipyconfig.utils import *
from sipyconfig.enums import *

if TYPE_CHECKING:
    from sipyconfig.card import SICard

si = SiUSBStation(mode_direct=True)

print(f"mode_master: {si._direct_mode}")
print(f"mode: {si.mode_string}")
print(f"control_number: {si.control_number}")
print("STATION_MODE", si.model_id_string)

temp_list: 'list[SICard]' = []
last_read = 0

try:
    while True:
        try:
            card = si.wait_for_si_card(0.25, auto_detect_srr=True)
            if abs(time.time() - last_read) > 0.25:
                for card1 in combine_punches(temp_list):
                    print_si_card(card1)
                temp_list.clear()
            if card is None:
                continue
            temp_list.append(card)
            last_read = time.time()
        except Exception as e:
            print(e)
finally:
    si.flush()
