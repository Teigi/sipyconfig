# pylint: skip-file
# type: ignore

from sipyconfig import SiUSBStation
from sipyconfig.utils import print_si_card

si = SiUSBStation(mode_direct=True)

print(f"mode_master: {si._direct_mode}")
print(f"mode: {si.mode_string}")
print(f"control_number: {si.control_number}")

si.ensure_control_readout(63, True)

print(f"mode_master: {si._direct_mode}")
print(f"mode: {si.mode_string}")
print(f"control_number: {si.control_number}")

try:
    while True:
        card = si.wait_for_si_card()
        print_si_card(card)
except:
    pass
si.flush()
