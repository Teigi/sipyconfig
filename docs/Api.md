# Code used in Spec / Program

## Protocol characters

Short hand | Description
-----------|------------
STX        | Start of text
ETX        | End of text
ACK        | Positive Handshake return
NAK        | Negative Handshake return
DLE        | DeLimitEr for data characters
WAKEUP     | 0xFF to wakeup station
-----------|------------
LEN        | length of parameter/data segment
CRC1, CRC0 | Checksum (2 bytes)

## Instruction Sets

Which    | Start | Byte1        | Byte2          | Segment        | CRC        | End
---------|-------|--------------|----------------|----------------|------------|------
Basic    | STX   | command code |                | parameter/data |            | ETX
Extended | STX   | command code | LEN            | parameter/data | CRC1, CRC0 | ETX

## Extended Commands

Hex    | Short Hand   | Description                              | Send Structure                  | Receive Structure
-------|--------------|------------------------------------------|---------------------------------|------------------
0x81   | GET_BACK_MEM | get data from backup memory              | ADR2, ADR1, ADR0, NUM           | CN1, CN0, ADR2, ADR1, ADR0, **&lt;NUM data bytes&gt;**
*0x82* | SET_SYS_DATA | set system data                          | ADR, **&lt;data bytes&gt;**     | CN1, CN0, ADR
0x83   | GET_SYS_DATA | get system data                          | ADR, NUM                        | CN1, CN0, ADR, **&lt;NUM data bytes&gt;**
*0xA2* | SRR_WRITE    | ShortRangeRadio - SysData write          | ??                              | ??
*0xA3* | SRR_READ     | ShortRangeRadio - SysData read           | ??                              | ??
*0xA6* | SRR_QUERY    | ShortRangeRadio - network device query   | ??                              | ??
*0xA7* | SRR_PING     | ShortRangeRadio - heartbeat, every 50sec | ??                              | ??
*0xA8* | SRR_ADHOC    | ShortRangeRadio - ad-hoc message         | ??                              | ??
0xB1   | GET_SI_5     | get SI-Card 5                            | <b>&lt;empty&gt;</b>            | CN1, CN0, **&lt;128 bytes&gt;**
*0xC3* | WRITE_SI_5   | write SI-Card 5 data page                | BN **&lt;16 bytes&gt;**         | ?? **( unknown )**
0xD3   | PUNCH_DATA   | transmit punch or data in auto send      | &lt;init by Station&gt;         | CN1, CN0, SI3, SI2, SI1, SI0, TD, TH, TL, TSS, DAT2, DAT1, DAT0
*0xE0* | CLEAR_SI     | clear SI-Card                            | <b>&lt;empty&gt;</b>            | ?? **( depreacted )**
0xE1   | GET_SI_6     | get SI-Card 6                            | BN                              | CN1, CN0, BN, **&lt;128 bytes&gt;**
0xE5   | DETECT_SI_5  | SI-5 detected                            | &lt;init by Station&gt;         | CN1, CN0, SI3, SI2, SI1, SI0
0xE6   | DETECT_SI_6  | SI-6 detected                            | &lt;init by Station&gt;         | Cn1, CN0, SI3, SI2, SI1, SI0
0xE7   | SI_REMOVED   | SI removed                               | &lt;init by Station&gt;         | CN1, CN0, SI3, SI2, SI1, SI0
0xE8   | DETECT_SI_8  | SI-8-11/p/t det.                         | &lt;init by Station&gt;         | CN1, CN0, SI3, SI2, SI1, SI0
*0xEA* | WRITE_SI_8   | write SI-Card 8-11/p/t                   | ADR, **&lt;4 bytes&gt;**        | CN1, CN0, ADR
0xEF   | GET_SI_8     | get SI-Card 8-11/p/t                     | BN                              | CN1, CN0, BN, **&lt;128 bytes&gt;**
0xF0   | SET_MS_MODE  | set MS-Mode                              | MS                              | CN1, CN0, MS
*0xF1* | GET_MS_MODE  | read MS-Mode                             | <b>&lt;empty&gt;</b>            | CN1, CN0, MS
0xF5   | ERASE_BACK   | Erase backup data                        | <b>&lt;empty&gt;</b>            | CN1, CN0
0xF6   | SET_TIME     | set time                                 | CD2, CD1, CD0, TD, TH, TL, TSS  | CN1, CN0, CD2, CD1, CD0, TD, TH, TL, TSS
0xF7   | GET_TIME     | get time                                 | <b>&lt;empty&gt;</b>            | CD2, CD1, CD0, TD, TH, TL, TSS
*0xF8* | TURN_OFF     | turn off station                         | <b>&lt;empty&gt;</b>            | CN1, CN0, 0xF8
*0xF9* | FEEDBACK     | acoustic/visual feedback from station    | NUM                             | CN1, CN0, NUM
0xFE   | SET_BAUDRATE | set baudrate                             | SPEED                           | CN1, CN0, SPEED


## Data Specs

Short hand         | Description
-------------------|------------
CN1, CN0           | Station code number (2 bytes)
ADR                | Memory Address (1 byte)
ADR2, ADR1, ADR0   | Memory Address (3 bytes)
NUM                | Number of bytes to read
SI3, SI2, SI1, SI0 | SI-Card Number (4 bytes)
CD2, CD1, CD0      | calendar: yy-mm-dd, binary format (3 bytes)
TD                 | day-of-week/half day
*bit5...bit4*      | 4 week counter relative
*bit3...bit1*      | day of week 000 -> Sunday, 110 -> Saturday
*bit0*             | 24h counter (0->am, 1->pm)
TH, TL             | 12h timer in seconds, binary (2 bytes)
TSS                | sub second values 1/256 sec
MEM2, MEM1, MEM0   | backup memory start address of the data record
DAT2, DAT1, DAT0   | Data bytes for SRR \<PUNCH_DATA\>-Command (see below for info)
DAT2               | Encodes the stations operating mode (e.a. Start, Control, ...)
DAT1, DAT0         | index of the punch on the SI-Card and total number of punches on Card
BN                 | block number from 0 to 8/SI6, 7/Other (8 for blocks 0, 4, 5, 6, 7)
MS                 | Master / Slave Mode
SPEED              | Baudrate 0 -> 4800, 1 -> 38400
-------------------|------------
CN                 | Station code number (1 byte)


## Basic Commands

Hex  | Short Hand         | Description   | Send Structure | Receive Structure
-----|--------------------|---------------|----------------|------------------
0x70 | SET_MS_MODE_BASIC  | Set MS-Mode   | MS             | CN, MS
0x7E | SET_BAUDRATE_BASIC | Set Baudrate  | SPEED          | CN, SPEED


## Mem Addresses

Hex  | Length | Short Hand         | Description
-----|--------|--------------------|------------
0x00 | 0x02   | OLD_SERIAL         | only up to BSx6, numbers &lt; 65.536
0x02 | 0x02   | OLD_CPU_ID         | only up to BSx6, numbers &lt; 65.536
0x00 | 0x04   | SERIAL_NO          | after BSx7, numbers &gt; 70.000
0x04 | 0x01   | SRR_CFG            | SRR-Dongle config
0x05 | 0x03   | FIRMWARE           | firmware version, ASCII code
0x08 | 0x03   | BUILD_DATE         | manufacturing date, YYMMDD
0x0B | 0x02   | MODEL_ID           | see Model IDs
0x0D | 0x01   | MEM_SIZE           | size of sys memory
0x15 | 0x03   | BAT_DATE           | date of battery change, YYMMDD
0x19 | 0x02   | BAT_CAP            | battery capacity, (multiply by 16/225 mAh)
0x1C | 0x02   | BACKUP_PTR_HI      | high bytes of backup memory pointer
0x21 | 0x02   | BACKUP_PTR_LO      | low bytes of backup memory pointer
0x33 | 0x01   | SI6_CB             | bitmask for SI Card 6 block readout, 0x00=0xC1 (0, 6, 7), 0x08=0xFF (all 8)
0x34 | 0x01   | SRR_CHANNEL        | SRR dongle frequency band, 0x00="red", 0x01="blue"
0x35 | 0x03   | USED_BAT_CAP       | used battery capacity, (multiply by 2.778e-5 to get precent used)
0x3D | 0x01   | MEM_OVERFLOW       | memory overflow if != 0x00
0x3F | 0x01   | SRR_PROTOCOL       | SRR protocol used, 0x00="basic", 0x01="AIR+"
0x50 | 0x02   | BAT_VOLT           | battery voltage, (multiply by 5/65536 V)
0x5A | 0x01   | SIAC               | should be 0x07 when SIAC on else 0x05, ( no effect noticed )
0x70 | 0x01   | PROGRAM            | PROGRAM bitmask
0x71 | 0x01   | MODE               | mode of station
0x72 | 0x01   | CONTROL_NUMBER     | control number of station
0x73 | 0x01   | SETTINGS_FLAG      | SETTINGS bitmask
0x74 | 0x01   | PROTOCOL_MODE_FLAG | CPC bitmask
0x75 | 0x03   | WAKEUP_DATE        | YYMMDD
0x78 | 0x03   | WAKEUP_TIME        | 1 byte DAY bitmask, 2 bytes seconds after midnight/midday
0x7B | 0x03   | SLEEP_TIME         | 1 byte DAY bitmask, 2 bytes seconds after midnight/midday
0x7E | 0x02   | ACTIVE_TIME        | station active in minutes, max 5759 (&lt; 96h)

## Modes

Hex  | Short hand         | Description
-----|--------------------|------------
0x02 | CONTROL
0x03 | START
0x04 | FINISH
0x05 | READOUT
0x06 | CLEAR_OLD          | without start-number ( deprecated )
0x07 | CLEAR
0x0A | CHECK
0x0B | PRINTOUT           | BS7_P Printer station (also used by SSR-Receiver-module)
0x0C | START_TRIG         | BS7_S with external trigger
0x0D | FINISH_TRIG        | BS7_S with external trigger
-----|--------------------|------------
0x31 | SIAC_TEST
0x32 | SIAC_CONTROL
0x33 | SIAC_START
0x34 | SIAC_FINISH
0x01 | SIAC_SPECIAL       | Indicates special function, read CONTROL_NUMBER and see "SIAC special modes"
-----|--------------------|------------
0x4D | MODE_DIRECT        | Indicates direct mode
0x53 | MODE_REMOTE        | Indicates remote mode

## SIAC special modes

Hex  | Short hand         | Description
-----|--------------------|------------
0x7B | SIAC_BATTERY_TEST
0x7C | SIAC_ON
0x7D | SIAC_OFF
0x7F | SIAC_RADIO_READOUT

## SIAC SRR Behaviour Modes

bitwise OR these codes with SIAC_CONTROL, SIAC_START, SIAC_FINISH for desired effect

Hex  | Short hand         | Description
-----|--------------------|------------
0x30 | NO_RADIO
0x70 | LAST_RECORD        | SIAC chip should send the last record over SRR
0xb0 | ALL_RECORDS        | SIAC chip should send all records over SRR
0xf0 | UNSENT_RECORDS     | SIAC chip should send all unsent records over SRR

## Bitmasks

Bits     | Description
---------|------------
---------| SRR_CFG
xxxxxx1x | auto send SIAC data
xxxxx1xx | sync time via radio
---------| PROGRAM
xx0xxxxx | competition
xx1xxxxx | training
---------| SETTINGS_FLAG
xxxxxxx1 | optical feedback
xxxxx1xx | acoustic feedback
11xxxxxx | highest bits of Control number
---------| PROTOCOL_MODE_FLAG
xxxxxxx1 | extended protocol
xxxxxx1x | auto send out
xxxxx1xx | handshake (only valid for card readout)
xxx1xxxx | access with password only
1xxxxxxx | read out SI-card after punch (only for punch modes; depends on bit 2: auto send out or handshake)
---------| DAY
xxxxxxx0 | relative to midnight/midday: 0 = am, 1 = pm 
xxxx000x | day of week: 000 = sunday, 110 = saturday
xx00xxxx | week counter 0-3, relative to programming date

## Model IDs

Bytes | Station                          | serial no.
------|----------------------------------|-----------
6F21  | SIMSSR1_AP                       |
8003  | BSF3                             | &gt; 1.000
8004  | BSF4                             | &gt; 10.000
8084  | BSM4_RS232                       |
8086  | BSM6_RS232 / BSM6_USB            |
8115  | BSF5                             | &gt; 30.000
8117  | BSF7                             | 70.000 - 70.521
8118  | BSF8                             | 72.002 - 72.009
8146  | BSF6                             | &gt; 30.000
8187  | BSF7_SI_MASTER                   |
8188  | BSF8_SI_MASTER                   |
8197  | BSF7                             | &gt; 71.000 \ {72.002 - 72.009}
8198  | BSF8                             | &gt; 80.000
9197  | BSM7_RS232 / BSM7_USB            |
9198  | BSM8_RS232 / BSM8_USB / BSM8-SRR |
9597  | BS7_S                            |
9D9A  | BS11_BL                          |
B197  | BS7_P                            |
B198  | BS8_P                            |
B897  | BS7_GSM                          |
CD9B  | BS11_BS_RED / BS11_BS_BLUE       |

## SI Card Structure

Short | Description
------|------------
CN2   | card number, byte 2
CN1   | card number, byte 1
CN0   | card number, byte 0
STD   | start time day
ST    | start time
STR   | start reserve
FTD   | finish time day
FT    | finish time
FTR   | finish reserve
CTD   | check time day
CT    | check time
LTD   | clear time day
LT    | clear time
LTR   | clear reserve
RC    | punch counter
P1    | first punch
PL    | punch data length
PM    | punch maximum
CN    | control number offset
PTD   | punchtime day offset
PTH   | punchtime high byte
PTL   | punchtime low byte
BC    | number of blocks on card
SPD   | start of personal data
EPD   | end of personal data
